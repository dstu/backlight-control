CC = gcc
CFLAGS = -Wall -Werror -g
LIBS = -lm

blc: blc.o control.o
	$(CC) $(LIBS) $(CFLAGS) -o blc blc.c control.o

blc.o: blc.c
	$(CC) $(CFLAGS) -c blc.c

control.o: control.c
	$(CC) $(CFLAGS) -c control.c

clean:
	rm -f blc blc.o control.o
