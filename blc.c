#include "control.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

static void print_usage(FILE *f, const char *binary_name) {
  fprintf(f, "Usage: %s <inc|dec|set> <value|value%%>\n", binary_name);
  fprintf(f, "   or: %s <get>         <actual|current|max>\n", binary_name);
}

static void print_options(FILE *f) {
}

static int is_help_arg(const char *s) {
  return (!strcmp(s, "-h")
          || !strcmp(s, "--help")
          || !strcmp(s, "-help"));
}

static int ends_with(const char *s, char c) {
  const int length = strlen(s);
  if (length < 1) {
    return 0;
  } else if (s[length - 1] == c) {
    return 1;
  } else {
    return 0;
  }
}

static int read_int(int *value, const char *s) {
  char *invalid = NULL;
  long int long_value = 0;
  errno = 0;

  long_value = strtol(s, &invalid, 10);
  if (errno) {
    fprintf(stderr, "Unable to decode value from %s\n", s);
    perror(NULL);
    return 0;
  } else if ('\0' != *invalid) {
    fprintf(stderr, "Caught trailing garbage \"%s\" in value \"%s\"\n", invalid, s);
    return 0;
  }

  if ((int)long_value != long_value) {
    fprintf(stderr, "Integer value %ld out of range\n", long_value);
    return 0;
  } else {
    *value = (int)long_value;
    return 1;
  }
}

static int read_percentage(int *percentage, const char *s) {
  int value = 0;

  if (read_int(&value, s)) {
    if (value < 0 || value > 100) {
      fprintf(stderr, "Invalid percentage %d (too %s)\n", value, value < 0 ? "small" : "large");
      return 0;
    }
    *percentage = (int)value;
    return 1;
  } else {
    return 0;
  }
}

static int tweak_brightness(const int sign, const char *arg) {
  const int length = strlen(arg);
  if (ends_with(arg, '%')) {
    char buffer[length - 1];
    strncpy(buffer, arg, length - 1);
    buffer[length - 1] = '\0';

    int percentage = 0;
    if (read_percentage(&percentage, buffer)) {
      return tweak_brightness_percent(sign * percentage);
    } else {
      return 0;
    }
  } else {
    int value = 0;
    if (read_int(&value, arg)) {
      return tweak_brightness_absolute(sign * value);
    } else {
      return 0;
    }
  }
}

static int increment_brightness(const char *arg) {
  return tweak_brightness(1, arg);
}

static int decrement_brightness(const char *arg) {
  return tweak_brightness(-1, arg);
}

int set_brightness(const char *s) {
  const int length = strlen(s);
  if (ends_with(s, '%')) {
    char buffer[length - 1];
    strncpy(buffer, s, length - 1);
    buffer[length - 1] = '\0';

    int percentage = 0;
    if (read_percentage(&percentage, s)) {
      return set_brightness_percent(percentage);
    } else {
      return 0;
    }
  } else {
    int value = 0;
    if (read_int(&value, s)) {
      return set_brightness_absolute(value);
    } else {
      return 0;
    }
  }
}

int main(int argc, char **argv) {
  int i, retval = 0;
  const char *action, *argument;

  for (i = 1; i < argc; i++) {
    if (is_help_arg(argv[i])) {
      print_usage(stdout, argv[0]);
      print_options(stdout);
      exit(0);
    }
  }

  if (argc != 3) {
    print_usage(stderr, argv[0]);
    exit(-1);
  }

  action = argv[1];
  argument = argv[2];
  if (!strcmp(action, "inc")) {
    retval = increment_brightness(argument) ? 0 : 2;
  } else if (!strcmp(action, "dec")) {
    retval = decrement_brightness(argument) ? 0 : 2;
  } else if (!strcmp(action, "set")) {
    retval = set_brightness(argument) ? 0 : 2;
  } else if (!strcmp(action, "get")) {
    if (!strcmp(argument, "max")) {
      int max_brightness = 0;
      if (get_max_brightness(&max_brightness)) {
        printf("%d\n", max_brightness);
      } else {
        retval = 2;
      }
    } else if (!strcmp(argument, "current")) {
      int brightness = 0;
      if (get_brightness(&brightness)) {
        printf("%d\n", brightness);
      } else {
        retval = 2;
      }
    } else if (!strcmp(argument, "actual")) {
      int actual_brightness = 0;
      if (get_actual_brightness(&actual_brightness)) {
        printf("%d\n", actual_brightness);
      } else {
        retval = 2;
      }
    } else {
      fprintf(stderr, "Unrecognized query argument \"%s\"\n", argument);
      retval = 1;
    }
  } else {
    fprintf(stderr, "Unrecognized action \"%s\"\n", action);
    retval = 1;
  }

  exit(retval);
}
