#include "control.h"
#include <stdio.h>
#include <errno.h>
#include <math.h>

static int read_int_from_file(int *value,
                              const char *path) {
  int result = 1;
  FILE *fp = NULL;
  errno = 0;

  fp = fopen(path, "r");
  if (NULL == fp) {
    fprintf(stderr, "Failed to open %s for reading: ", path);
    perror(NULL);
    result = 0;
    goto end;
  }

  fscanf(fp, "%d", value);
  if (errno) {
    fprintf(stderr, "Unable to read brightness from %s: ", path);
    perror(NULL);
    result = 0;
  }

  fclose(fp);
  if (errno) {
    fprintf(stderr, "Failed to close %s: ", path);
    perror(NULL);
    result = 0;
  }

 end:
  return result;
}

static int write_int_to_file(int value, const char *path) {
  int result = 1;
  FILE *fp = NULL;
  errno = 0;

  fp = fopen(path, "w");
  if (NULL == fp) {
    fprintf(stderr, "Failed to open %s for writing: ", path);
    perror(NULL);
    result = 0;
    goto end;
  }

  fprintf(fp, "%d", value);
  if (errno) {
    fprintf(stderr, "Unable to write value to %s: ", path);
    perror(NULL);
    result = 0;
  }

  fclose(fp);
  if (errno) {
    fprintf(stderr, "Failed to close %s: ", path);
    perror(NULL);
    result = 0;
  }

 end:
  return result;
}

static int sign(const int x) {
  return x < 0 ? -1
    :    x > 0 ? 1
    :            0;
}

int get_max_brightness(int *value) {
  return read_int_from_file(value, MAX_BRIGHTNESS_PATH);
}

int get_actual_brightness(int *value) {
  return read_int_from_file(value, ACTUAL_BRIGHTNESS_PATH);
}

int get_brightness(int *value) {
  return read_int_from_file(value, BRIGHTNESS_PATH);
}

int set_brightness_absolute(const int value) {
  int max_brightness = 0;

  if (value < 0) {
    fprintf(stderr, "Cannot set brightness: brightness %d is less than zero\n", value);
    return 0;
  }

  if (!get_max_brightness(&max_brightness)) {
    /* fprintf(stderr, "Cannot set brightness: failed to read max brightness\n"); */
    return 0;
  }

  if (value > max_brightness) {
    fprintf(stderr, "Cannot set brightness: brightness %d is greater than max brightness of %d\n", value, max_brightness);
    return 0;
  }
  
  if (!write_int_to_file(value, BRIGHTNESS_PATH)) {
    /* fprintf(stderr, "Failed to set brightness\n"); */
    return 0;
  }

  return 1;
}

int set_brightness_percent(const int percentage) {
  int max_brightness = 0;
  const float multiplier = percentage / 100.0f;

  if (percentage < 0 || percentage > 100) {
    fprintf(stderr, "Cannot set brightness: %d is not a valid percentage\n", percentage);
    return 0;
  }

  if (!get_max_brightness(&max_brightness)) {
    fprintf(stderr, "Cannot set brightness: unable to read max brightness\n");
    return 0;
  }

  return set_brightness_absolute(multiplier * max_brightness);
}

int tweak_brightness_absolute(const int amount) {
  int max_brightness = 0, new_brightness = 0;

  if (!get_max_brightness(&max_brightness)) {
    /* fprintf(stderr, "Cannot tweak brightness: failed to read max brightness\n"); */
    return 0;
  }

  if (!get_brightness(&new_brightness)) {
    /* fprintf(stderr, "Cannot tweak brightness: failed to read current brightness\n"); */
    return 0;
  }
  new_brightness += amount;

  if (new_brightness < 0 || new_brightness > max_brightness) {
    if (amount < 0) {
      new_brightness = 0;
    } else if (amount > 0) {
      new_brightness = max_brightness;
    }
  }

  if (!set_brightness_absolute(new_brightness)) {
    /* fprintf(stderr, "Cannot tweak brightness: failed to set new brightness\n"); */
    return 0;
  }

  return 1;
}

int tweak_brightness_percent(const int percentage) {
  int jump_size = 0,
    current_brightness = 0,
    max_brightness = 0,
    next_step = 0;

  if (percentage < -100 || percentage > 100) {
    fprintf(stderr, "Cannot tweak brightness: %d isn't a valid percentage\n", percentage);
    return 0;
  }

  if (0 == percentage) {
    return 1;
  }

  if (!get_brightness(&current_brightness)) {
    /* fprintf(stderr, "Cannot tweak brightness: can't read current brightness\n"); */
    return 0;
  }

  if (!get_max_brightness(&max_brightness)) {
    /* fprintf(stderr, "Cannot tweak brightness: can't read max brightness\n"); */
    return 0;
  }

  jump_size = (int)(fabs(percentage / 100.0f) * max_brightness);
  next_step = current_brightness + sign(percentage) * (jump_size + current_brightness % jump_size);
  if (next_step > max_brightness || next_step < 0) {
    next_step = percentage < 0 ? 0 : max_brightness;
  }
  return set_brightness_absolute(next_step);
}

