#ifndef backlight_control_control_h
#define backlight_control_control_h

#ifndef BRIGHTNESS_FILE
#define BRIGHTNESS_FILE "brightness"
#endif

#ifndef ACTUAL_BRIGHTNESS_FILE
#define ACTUAL_BRIGHTNESS_FILE "actual_brightness"
#endif

#ifndef MAX_BRIGHTNESS_FILE
#define MAX_BRIGHTNESS_FILE "max_brightness"
#endif

#ifndef BRIGHTNESS_DEVICE_PATH
#define BRIGHTNESS_DEVICE_PATH "/sys/class/backlight/pwm-backlight.0"
#endif

#define BRIGHTNESS_PATH (BRIGHTNESS_DEVICE_PATH "/" BRIGHTNESS_FILE)

#define ACTUAL_BRIGHTNESS_PATH (BRIGHTNESS_DEVICE_PATH "/" ACTUAL_BRIGHTNESS_FILE)

#define MAX_BRIGHTNESS_PATH (BRIGHTNESS_DEVICE_PATH "/" MAX_BRIGHTNESS_FILE)

/*
  Reads the backlight's max brightness into the supplied
  integer. Returns 0 on failure, 1 on success.
 */
int get_max_brightness(int *value);

/*
  Reads the actual backlight brightness into the supplied
  integer. It isn't clear how this is different from the
  brightness. Returns 0 on failure, 1 on success.
 */
int get_actual_brightness(int *value);

/*
  Reads the backlight brightness into the supplied integer. It
  isn't clear how this is different from the brightness. Returns
  0 on failure, 1 on success.
 */
int get_brightness(int *value);

/*
  Writes the supplied brightness value to the backlight
  brightness setting. This value must be nonnegative and not
  greater than the max brightness. Returns 0 on failure, 1 on
  success.
*/
int set_brightness_absolute(const int value);

int set_brightness_percent(const int percentage);

/*
  Modifies the current backlight brightness by summing its
  argument with the supplied increment, which may be
  negative. The resulting value is safely rounded up to zero or
  down to the max brightness. Returns 0 on failure, 1 on
  success.
*/
int tweak_brightness_absolute(const int amount);

/*
  Modifies the current backlight brightness by summing its
  argument with the supplied increment, which may be
  negative. The resulting value is safely rounded up to zero or
  down to the max brightness. Returns 0 on failure, 1 on
  success.
*/
int tweak_brightness_percent(const int percentage);

#endif // backlight_control_control_h
